var ptnName = document.getElementById('ptnName')
var ptnIC = document.getElementById('ptnIC')
var ptnAdd = document.getElementById('ptnAdd')
var ptnTel = document.getElementById('ptnTel')
var ptnVAS = document.getElementById('ptnVAS')

var savedPtnName = document.getElementById('savedPtnName');
var savedPtnIC = document.getElementById('savedPtnIC');
var savedPtnAdd = document.getElementById('savedPtnAdd');
var savedPtnTel = document.getElementById('savedPtnTel');
var savedPtnVAS = document.getElementById('savedPtnVAS');


function savePtnName(){
  if (ptnName.value.length <1)return;
  savedPtnName.innerHTML = ptnName.value;
  ptnName.value = '';
  localStorage.setItem('savedPtnName', savedPtnName.innerHTML);
}

function savePtnIC(){
  if (ptnIC.value.length <1)return;
  savedPtnIC.innerHTML = ptnIC.value;
  ptnIC.value = '';
  localStorage.setItem('savedPtnIC', savedPtnIC.innerHTML);
}

function savePtnAdd(){
  if (ptnAdd.value.length <1)return;
  savedPtnAdd.innerHTML = ptnAdd.value;
  ptnAdd.value = '';
  localStorage.setItem('savedPtnAdd', savedPtnAdd.innerHTML);
}

function savePtnTel(){
  if (ptnTel.value.length <1)return;
  savedPtnTel.innerHTML = ptnTel.value;
  ptnTel.value = '';
  localStorage.setItem('savedPtnTel', savedPtnTel.innerHTML);
}

function savePtnVAS(){
  if (ptnVAS.value.length <1)return;
  savedPtnVAS.innerHTML = ptnVAS.value;
  ptnVAS.value = '';
  localStorage.setItem('savedPtnVAS', savedPtnVAS.innerHTML);
}

// Check for saved wishlist items
var saved1 = localStorage.getItem('savedPtnName');
var saved2 = localStorage.getItem('savedPtnIC');
var saved3 = localStorage.getItem('savedPtnAdd');
var saved4 = localStorage.getItem('savedPtnTel');
var saved5 = localStorage.getItem('savedPtnVAS');

// If there are any saved items, update our list

savedPtnName.innerHTML = saved1;
savedPtnIC.innerHTML = saved2;
savedPtnAdd.innerHTML = saved3;
savedPtnTel.innerHTML = saved4;
savedPtnVAS.innerHTML = saved5;

// List Ubat

var listUbatInput = document.getElementById('listUbatInput');
var listUbat = document.getElementById('listUbat');
function saveListUbat(){
  if (listUbatInput.value.length <1)return;
  listUbat.innerHTML += '<li>' + listUbatInput.value + ' <button onclick="deleteItem(this)">Delete</delete>' + '</li>';
  listUbatInput.value = '';
  localStorage.setItem('listUbat', listUbat.innerHTML);
}


function deleteItem(elementToDelete){
  elementToDelete.parentElement.remove()
  localStorage.setItem('listUbat', listUbat.innerHTML);
}

var saved6 = localStorage.getItem('listUbat');
listUbat.innerHTML = saved6;



// Tarikh Ubat TCA

var ubatTcaInput = document.getElementById('ubatTcaInput');
var ubatTca = document.getElementById('ubatTca');
function saveUbatTca(){
  if (ubatTcaInput.value.length <1)return;
  ubatTca.innerHTML += '<li>' + ubatTcaInput.value + ' <button onclick="deleteItem(this)">Delete</delete>' + '</li>';
  ubatTcaInput.value = '';
  localStorage.setItem('ubatTca', ubatTca.innerHTML);
}


function deleteItem(elementToDelete){
  elementToDelete.parentElement.remove()
  localStorage.setItem('ubatTca', ubatTca.innerHTML);
}

var saved7 = localStorage.getItem('ubatTca');
ubatTca.innerHTML = saved7;


// Tarikh Klinik TCA


var TCAInput = document.getElementById('TCAInput');
var TCA = document.getElementById('TCA');
function saveTCA(){
  if (TCAInput.value.length <1)return;
  TCA.innerHTML += '<li>' + TCAInput.value + ' <button onclick="deleteItem(this)">Delete</delete>' + '</li>';
  TCAInput.value = '';
  localStorage.setItem('TCA', TCA.innerHTML);
}


function deleteItem(elementToDelete){
  elementToDelete.parentElement.remove()
  localStorage.setItem('TCA', TCA.innerHTML);
}

var saved8 = localStorage.getItem('TCA');
TCA.innerHTML = saved8;



function clearList(){
  localStorage.clear();
}


function refreshPage(){
  window.location.reload();
}
